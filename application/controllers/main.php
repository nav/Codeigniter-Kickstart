<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Main extends CI_Controller {

		public function index()
		{
			$this->load->view('inc/header');
			$this->load->view('main_page');
			$this->load->view('inc/footer');
		}

		public function about()
		{
			$this->load->view('inc/header');
			$this->load->view('pages/about');
			$this->load->view('inc/footer');
		}

		public function services()
		{
			$this->load->view('inc/header');
			$this->load->view('pages/services');
			$this->load->view('inc/footer');
		}

		public function contact()
		{
			$this->load->view('inc/header');
			$this->load->view('pages/contact');
			$this->load->view('inc/footer');
		}
	}
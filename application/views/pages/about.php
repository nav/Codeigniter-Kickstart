

<!-- Page Content -->
    <div class="container">

        <div class="row">
            <div class="col-lg-12 text-center">
                <h1>About Page</h1>
<pre>
    Under the main controller there is a function calling for this about page.
    
    Reflects under the url as: http://localhost/codeigniter/main/about
    
 
    $this->load->view('inc/header');
    $this->load->view('pages/about');
    $this->load->view('inc/footer');


</pre>
            </div>
        </div>
        <!-- /.row -->

    </div>
<!-- /.container -->
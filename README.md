Codeigniter-Kickstarter
=======================

This is a personal flavored Starterkit for my Codeigniter 
Projects. Includes a few sample pages loaded with bootstrap.


# Includes:
* Codeigniter 2.2.0
* Bootstrap 3.2.0
* jQuery v1.11.0
* Codeigniter user guide

# Good to know:
* Put your custom CSS under assets/css/custom.css
* Put your custom JS under assets/js/custom.js
* URL helper is autoloaded so you can keep your links dynamic with base_url / site_url

# Clean Urls:
- Check your config.php for Base_url & remove the index_page parameter
- Check on your webserver (or local env) if the mod_rewrite module is activated
- Use this .htaccess snippet in your root of application 


```
RewriteEngine on
RewriteCond $1 !^(index\.php|resources|robots\.txt)
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)$ index.php/$1 [L,QSA]
```


* Check these 3 points and this should work!

